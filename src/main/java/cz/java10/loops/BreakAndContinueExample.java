package cz.java10.loops;

import java.util.Scanner;

public class BreakAndContinueExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int cislo;
        do {
            cislo = scanner.nextInt();
            if (cislo<0){
                break; // sposobí že sa okamžite ukončí cyklus
            }
            if(cislo > 20){
                System.out.println("Vypocet muze trvat dlouho preskakuji...");
                continue;// zpusobi ze vykonavani se presune na zacatek cyklu
            }
            long x = 1;
            for (int i = 1; i <= cislo; i++) {
                x *= i;

            }
            System.out.println(cislo + "!=" + x);
        } while (cislo > 0);
    }
}
