package cz.java10.loops;

import java.util.Scanner;

public class WhileLoopExample {
    public static void main(String[] args) {
        int n = 2;
        while (n < 20) {
            System.out.println(n);
            n += 2;
        }

        Scanner scanner = new Scanner(System.in);
        int vst = scanner.nextInt();
        while (vst != 0) {
            System.out.println(vst + "*" + vst + "=" + (vst * vst));
            vst = scanner.nextInt();
        }



        int a = 5;
        String s = "ahoj";
        System.out.println(s);//vypise ahoj
        System.out.println("s");//vypise s
    }
}
