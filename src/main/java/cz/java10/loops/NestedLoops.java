package cz.java10.loops;

import java.util.Scanner;

public class NestedLoops {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int cislo;
        do {
            cislo = scanner.nextInt();
            if (cislo<0){
                break; // sposobí že sa okamžite ukončí cyklus
            }
            long x = 1;
            for (int i = 1; i <= cislo; i++) {
                x *= i;

            }
            System.out.println(cislo + "!=" + x);
        } while (cislo > 0);

    }
}
