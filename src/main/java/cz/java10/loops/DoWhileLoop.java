package cz.java10.loops;

import java.util.Scanner;

public class DoWhileLoop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int vst=0;
        do {
            vst = scanner.nextInt();
            System.out.println(vst + "*" + vst + "=" + (vst * vst));
        } while (vst != 0);

        vst=0;
        if(vst!=0) {
            do {
                vst = scanner.nextInt();
                System.out.println(vst + "*" + vst + "=" + (vst * vst));
            } while (vst != 0);
        }
    }
}
