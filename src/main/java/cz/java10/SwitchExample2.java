package cz.java10;

import java.util.Scanner;

public class SwitchExample2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadej cislo mesice");

        int mNumber = scanner.nextInt();
        switch (mNumber) {
            case 1:
                System.out.println("Leden");
                break;
            case 2:
                System.out.println("Unor");
                break;
            case 3: {
                System.out.println("Brezen");
            }
            break;
            default:
                System.out.println("Neznamy mesic");
        }

        switch (mNumber) {
            case 12: // ||
            case 1: //  ||
            case 2: //  ||
            case 3: //  ||
                System.out.println("Zima");
                break;
            case 4:
            case 5:
            case 6:
                System.out.println("Jaro");
                break;
            default:
                System.out.println("Nezname rocni obdobi");
                return;
        }


        //java 17
        switch (mNumber) {
            case 1 -> {
                System.out.println("Leden");
            }//neni break ale stejne se nevykona pokud mNumber nebude 2, switch od javy 17
            case 2 -> System.out.println("Unor");
        }
//java 17
        String nazevMesice = switch (mNumber) {
            case 1 -> "Leden";
            case 2 -> {
                System.out.println("nejaky kontrolni vypis");
                //...
                //...
                yield "Unor";
            }
            case 3, 4 -> "Brezen nebo duben";
            default -> "Neznamy";
        };

    }
}
