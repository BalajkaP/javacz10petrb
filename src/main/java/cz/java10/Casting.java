package cz.java10;

public class Casting {
    public static void main(String[] args) {
        int i = 25;
        long j = i;

        char c = 'a';
        int intC = c;

        float f = 99.9F;
        int intF = (int) f;
        System.out.println(intF);
        int intFrounded = Math.round(f);
        System.out.println(intFrounded);

        String s = "23";
        int intS = Integer.parseInt(s);
        int inc = intS + 2;
        System.out.println(inc);//25
    }
}
