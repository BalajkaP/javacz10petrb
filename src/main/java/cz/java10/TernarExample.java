package cz.java10;

import java.util.Scanner;

public class TernarExample {
    public static void main(String[] args) {

        //podminka ? trueVyraz : falseVyraz;

        Scanner s = new Scanner(System.in);
        int a = s.nextInt();
        int b = s.nextInt();
        int max;

        if (a > b) {
            max = a;
        } else {
            max = b;
        }

        max = (a > b) ? a : b;      // stejne jako radek 15-19
    }
}
