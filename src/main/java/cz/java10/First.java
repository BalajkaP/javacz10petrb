package cz.java10;

public class First {
    final double MY_PI = 3.14;
    public int aa;


    public static void main(String[] args) {
        final int ss;
        int i = 0;
        i = i + 1;//i bude 1
        i++;//i bude 2
        ++i;
        i--;
        double mojePi = Math.PI;

        int hex = 0xF;//15

        // byte aaaa =(byte) 100000L;
        float aa = 1.0F;
        System.out.println(2_54555455672L);
        ss = 1;


        final boolean isSunnyDay = false;
        boolean isSunday = true;

        boolean negIsSunnyDay = !isSunnyDay;//NOT
        boolean orResult = true || false;//true
        boolean andResult = true && false && true;//false

        char oneChar = 's' + 1;
        char lowerA = 97;
        System.out.println(lowerA);

        boolean isUpperChar = 's' > 97 && 's' < 122;
        System.out.println(isUpperChar);
        String ssss = "s";

        String withEscape = "ahoj\ndale";
        System.out.println("Hello \nword");

        String concated = "jeden" + " " + "druhy";
        System.out.println(concated);
    }
}
