package cz.java10;

public class ExponentDeclarationExample {
    public static void main(String[] args) {
        double a = 1E10;
        long l = (long)a;
        float f1 = 1000000000F;
        float f2 = 0.000000001F;
        System.out.println(f1+f2);
        System.out.println(l);
    }
}
