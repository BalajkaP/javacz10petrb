package cz.java10;

import java.util.Scanner;

public class SwitchExample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Zadaj číslo planéty");
        int a = scanner.nextInt();
        switch (a) {
            case 1:
                System.out.println("Merkur");
                break;

            case 2:
                System.out.println("Venuša");
                break;

            case 3:
                System.out.println("Zem");
                break;

            default:
                System.out.println("Iná planéta");
        } //switch
    } //main
}
