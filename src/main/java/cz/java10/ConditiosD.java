package cz.java10;

import java.util.Scanner;

public class ConditiosD {
    public static void main(String[] args) {
        Scanner vstup = new Scanner(System.in);
        System.out.println("zadajte znak");
        char znak = vstup.nextLine().charAt(0);
        if (znak >= 97 && znak <= 122) {
            System.out.println(znak + " je to male pismeno");
        } else if (znak >= 65 && znak <= 90) {
            System.out.println(znak + " je to velke pismeno");
        } else {
            System.out.println("nejde o pismeno!");
        }
    }
}
