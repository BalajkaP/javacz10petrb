package cz.java10;

public class Operators {
    static int b = 1;


    public static void main(String[] args) {
        int a = 5;
        a = a + 1;//6
        a += 1;//same as above a = 6 +1 = 7
        // += *= /= -=
        a *= 2;//14 a = a * 2
        a = b;//1
        a += b;//2

        //****************modulo and divide********
        int m = 10 % 3;//1 10/3=3 zbytek 1
        int test = 20;
        boolean isTestEven = (test % 2) == 0;
        boolean t2 = test != m;
        System.out.println(isTestEven);

        int div = 7 / 2;//3
        double div2 = 7 / 2.0;//3.5
        int aa = 7;
        int b = 2;
        double div3 = (double) aa / b;//3.5

     //   System.out.println("******************XOR");
        System.out.println(Integer.toBinaryString(17)+ " " + 17);
        System.out.println(Integer.toBinaryString(22)+ " " + 22);
        System.out.println("  "+ Integer.toBinaryString(17 ^ 22) + " " + (17 ^ 22));
        System.out.println();

        System.out.println("******************OR");
        System.out.println(Integer.toBinaryString(17)+ " " + 17);
        System.out.println(Integer.toBinaryString(22)+ " " + 22);
        System.out.println(Integer.toBinaryString(17 | 22) + " " + (17 | 22));
        System.out.println();

        System.out.println("******************AND");
        System.out.println(Integer.toBinaryString(17)+ " " + 17);
        System.out.println(Integer.toBinaryString(22)+ " " + 22);
        System.out.println(Integer.toBinaryString(17 & 22) + " " + (17 & 22));

        boolean isVisible=true;
        boolean isDry = false;
        boolean isWet = true;

        final int VISIBLE_FLAG = 1;
        final int DRY_FLAG = 2;
        final int WET_FLAG = 4;

        int combinedBoolean=0;
        combinedBoolean = combinedBoolean | VISIBLE_FLAG;
        //bin combinedFlag 00001
        boolean isVisibleSet = (combinedBoolean & VISIBLE_FLAG) == VISIBLE_FLAG;


        combinedBoolean = combinedBoolean | DRY_FLAG;
        boolean isDrySet = (combinedBoolean & DRY_FLAG) == DRY_FLAG;

    }
}
